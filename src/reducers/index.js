import { combineReducers } from 'redux';
import modal from './modal';
import select from './select';

const rootReducer = combineReducers({
    modal,
    select,
});

export default rootReducer;