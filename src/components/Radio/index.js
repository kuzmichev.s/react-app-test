import React, { Component } from 'react';
import './index.css';

class Radio extends Component {
    render () {
        return (
            <div>
                <input type="radio" name={this.props.name} id={this.props.id} value={this.props.value} checked={this.props.checked} onChange={this.props.onChange} />
                <label htmlFor={this.props.id}>{this.props.label}</label>
            </div>
        );
    }
}

export default Radio;