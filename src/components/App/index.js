import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as AppActions from '../../actions';
import './index.css';
import Button from '../Button';
import Modal from '../Modal';

class App extends Component {
  render() {
    return (
      <div className="App">
          <Button title="Open modal" onClick={this.props.actions.toggleModal} />
          <Modal />
      </div>
    );
  }
}

const mapStateToProps = (store) => ({
    modal: store.modal,
});

const mapDispatchToProps = (dispatch) => ({
  actions: bindActionCreators(AppActions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
